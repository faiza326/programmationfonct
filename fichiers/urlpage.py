import time
from bs4 import BeautifulSoup 
from urllib.request import urlopen
import csv

class Urlpage:
    
    def __init__(self):
        pass

    def get_pages(token, nb):
        pages = []
        pages = list(map(lambda i: token + str(i), range(1,nb+1)))
        return pages

    def urlFct(pages):
        return list(map(lambda i: urlopen(i), pages))

    def Fct_BeautifulSoup(response,type1):
        return BeautifulSoup(response,type1)

    def SoupFindAll(code,balise, attribut,valeur):
        return code.find_all(balise,{attribut:valeur})

    def SoupFindAtr(code,balise, attribut,valeur):
        return code.find(balise,{attribut:valeur})
    
    def SoupFind(code,balise):
        return code.find(balise)

    # def get_info(response):

    #     file = open('donnees.csv', 'w',encoding='utf8',errors='ignore')
    #     writer = csv.writer(file)
    #     # write title row
    #     writer.writerow(["Ville", "Prix", "Superficie m²", "Piéces"])


    #     for res in response:
    #             soup = A.Fct_BeautifulSoup(res, 'html.parser')
    #             em_box = A.SoupFindAll(soup,"div","class","offer-details")

    #             for channel in em_box:
                    
    #                 Ville = A.SoupFindAtr(channel,"span","class","offer-details-location--locality")
    #                 Prix = (A.SoupFind(channel,"span")).text
    #                 Superficie = A.SoupFindAtr(channel,"span","class","offer-area-number") 
    #                 NbrChambre = (A.SoupFindAtr(channel,"span","class","offer-rooms-number")).text 
    #                 writer.writerow([Ville, Prix, Superficie, NbrChambre])
       
    #         #     Ville=map(lambda channel:A.SoupFindAtr(channel,"span","class","offer-details-location--locality"),em_box)
    #         #     # Ville = A.SoupFindAtr(channel,"span","class","offer-details-location--locality")
    #         # Prix=map(lambda channel:(A.SoupFind(channel,"span")).text,em_box)
    #         #     # Prix = (A.SoupFind(channel,"span")).text
    #         # Superficie=map(lambda channel:A.SoupFindAtr(channel,"span","class","offer-area-number"),em_box)
    #         #     # Superficie = A.SoupFindAtr(channel,"span","class","offer-area-number") 
    #         #     # NbrChambre = (A.SoupFindAtr(channel,"span","class","offer-rooms-number")).text 
    #         # NbrChambre=map(lambda channel:(A.SoupFindAtr(channel,"span","class","offer-rooms-number")).text,em_box)
    #     file.close()
  