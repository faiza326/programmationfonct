from urlpage import Urlpage as A
from nettoyage import Nettoyage as B
from fichier import Fich as F
from calcul import Calcul as C
import pandas as pd
from urllib.request import urlopen
import csv
import numpy as np
from functools import reduce
import matplotlib.pyplot as plt
import pylab
import seaborn as sns

class Main:

    def _init_(self):
        pass
    def main(): 
       
        token = 'https://www.logic-immo.com/vente-immobilier-marseille-tous-codes-postaux,paris-75,nord-59,421_99,100_1,84_1/options/groupprptypesids=1,2,6,7,12,15/page='
        pages = A.get_pages(token,2)
        response = A.urlFct(pages)

        file = open('donnees.csv', 'w',encoding='utf8',errors='ignore')
        writer = csv.writer(file)
        # write title row
        writer.writerow(["Ville", "Prix", "Superficie m²", "Piéces"])

        F.get_info(response)

        df = pd.read_csv('donnees.csv')
        df.columns = ["Ville", "Prix", "Superficie m²", "Piéces"]
        if not df.empty :
            df['Prix'] = B.Remplacer('€','',df['Prix'])
            df['Prix'] = B.Remplacer(' ','',df['Prix'])
            df['Prix'] = df['Prix'].astype(float)  
            df['Ville']= B.Remplacer('<span class="offer-details-location--locality">','',df['Ville'])
            df['Ville']= B.Remplacer('</span>','',df['Ville'])
            df['Superficie m²']= B.Remplacer('<span class="offer-area-number">','',df['Superficie m²'])
            df['Superficie m²']= B.Remplacer('</span>','',df['Superficie m²'])
            df['Superficie m²'] = df['Superficie m²'].astype(float) 
            df['Piéces']= B.Remplacer('<span class="offer-rooms-number">','',df['Piéces'])
            df['Piéces']= B.Remplacer('</span>','',df['Piéces'])
            df['Piéces'] = df['Piéces'].astype(float) 
          
            #Supprimer les NaN
            B.DeleteNan(df) 

            df1,df2=F.get_data(df)
            df=df1
            #list=pd.unique(df["ville"]).tolist()
            valeur=df.iloc[:,1].unique()
            print(valeur)
            dfMean=df.groupby('Ville' , as_index=False).mean()
            fic=dfMean.columns[1]
            dfMean=dfMean.drop([fic],axis=1)
            print(dfMean.iloc[0:len(dfMean),0])
            print(dfMean)

            #calcul Moyenne Mediane
            print ("Moyenne des Superficie est : ", C.moyenne(dfMean['Superficie m²']))
            print ("Moyenne des Prix est : ", C.moyenne(dfMean['Prix']))
            print ("La mediane de la Superficie est : ", C.mediane(dfMean['Superficie m²']))

            #calcul correlation
            df1 = pd.DataFrame(np.random.randn(1000, 4), columns=['Prix', 'Superficie m²', 'Piéces','Ville'])
            print (C.correlation(df1,'spearman'))

            #selection de la première colonne de notre dataset
            x = dfMean.iloc[0:len(dfMean),1]
            # #selection de deuxième colonnes de notre dataset 
            y = dfMean.iloc[0:len(dfMean),2] 
            # estimating coefficients 
            b = C.estimate_coef(x, y) 
            print("Les coefficients estimés:\nb_0 = {} \n b_1 = {}".format(b[0], b[1])) 

            #plotting 
            C.plot_regression_line(x, y, b,x.name,y.name) 
            C.NuagePoint(x,y)
            x1 = dfMean.iloc[0:len(dfMean),0]
            y1 = dfMean.iloc[0:len(dfMean),3]
            C.barPlot(dfMean,x1,y1)
            dfMean["Ville"].value_counts(normalize=True).plot(kind='pie')
            plt.show()
            sns.heatmap(dfMean.corr(), annot=True, cmap='Greens')
            plt.title("Matrice de corrélation entre les différentes caractéristiques des annonces\n", fontsize=18, color='#009432')
            plt.show()


        else:
            print('Le dataframe est vide echec de la récupération de données')

    main()