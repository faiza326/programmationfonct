import pandas as pd

class Nettoyage:
    def __init__(self):
        pass
    def Remplacer(caracter1,caracter2,colonne):
        return colonne.str.replace(caracter1,caracter2)
    
    #Supprimer les NaN
    def DeleteNan(df):
        return  df.dropna(inplace=True)
    
    def ToNumeric(df):
        return  pd.to_numeric(df)
