import pandas as pd
import time
from bs4 import BeautifulSoup 
import csv
from urllib.request import urlopen
import numpy as np
import matplotlib.pyplot as plt

token = 'https://www.logic-immo.com/vente-immobilier-marseille-tous-codes-postaux,paris-75,nord-59,421_99,100_1,84_1/options/groupprptypesids=1,2,6,7,12,15/page='

def get_pages(token, nb):
    pages = []
    for i in range(1,nb+1):
        j = token + str(i)
        pages.append(j)
    return pages

pages = get_pages(token,1)

response=[]
for i in pages: 
    response.append(urlopen(i))

def SoupFindAll(code,balise, attribut,id):
    return code.find_all(balise,{attribut:id})

def SoupFind(code,balise, attribut,id):
    return code.find_all(balise,{attribut:id})

file = open('donnees.csv', 'w',encoding='utf8',errors='ignore')
writer = csv.writer(file)
# write title row
writer.writerow(["Ville", "Prix", "Superficie m²", "Piéces"])

for res in response:
    soup = BeautifulSoup(res, 'html.parser')
    em_box = SoupFindAll(soup,"div","class","offer-details")

    for channel in em_box:

        Ville= channel.find('span',{"class":"offer-details-location--locality"})
        Prix = channel.find("span").text
        Superficie = channel.find('span',{"class":"offer-area-number"}).text
        NbrChambre = channel.find("span",{"class":"offer-rooms-number"}).text
        # print Departement + ' ' + Prix + ' ' + Surface
        writer.writerow([Ville, Prix, Superficie, NbrChambre])

file.close()

df = pd.read_csv('donnees.csv')
df.columns = ["Ville", "Prix", "Superficie m²", "Piéces"]

def Remplacer(caracter1,caracter2,colonne):
    return colonne.str.replace(caracter1,caracter2)

df['Prix'] = Remplacer('€','',df['Prix'])
df['Prix'] = Remplacer(' ','',df['Prix'])
df['Ville']= Remplacer('<span class="offer-details-location--locality">','',df['Ville'])
df['Ville']= Remplacer('</span>','',df['Ville'])

#Supprimer les NaN
df.dropna(inplace=True)

#exporter le df dans un nouveau fichier
df.to_csv('donnees_clean.csv')
print(df)

#fonction qui calcule la moyenne 
def moyenne(df):
    return np.mean(df)

def correlation(df,methode):
    return df.corr(method=methode)

def mediane(df):
    return np.median(df)

print ("Moyenne des Superficie est : ", moyenne(df['Superficie m²']))
print ("Moyenne des Prix est : ", moyenne(df['Prix']))
print ("La mediane de la Superficie est : ", mediane(df['Superficie m²']))

df1 = pd.DataFrame(np.random.randn(1000, 4), columns=['Prix', 'Superficie m²', 'Piéces','Ville'])
print (correlation(df1,'spearman'))

#selection de la première colonne de notre dataset (la taille de la population)
x = df.iloc[0:len(df),3]
print(x)
# #selection de deuxième colonnes de notre dataset (le profit effectué)
y = df.iloc[0:len(df),2] 

# axes = plt.axes()
# axes.grid() # dessiner une grille pour une meilleur lisibilité du graphe
# plt.scatter(X,Y) # X et Y sont les variables qu'on a extraite dans le paragraphe précédent
# plt.show()

#linregress() renvoie plusieurs variables de retour. On s'interessera 
# particulierement au slope et intercept
# slope, intercept, r_value, p_value, std_err = stats.linregress(X, Y)

# def predict(x):
#    return slope * x + intercept

# #la variable fitLine sera un tableau de valeurs prédites depuis la tableau de variables X
# fitLine = predict(X)
# plt.plot(X, fitLine, c='r')
  
def estimate_coef(x, y): 
    # number of observations/points 
    n = np.size(x) 
  
    # mean of x and y vector 
    m_x, m_y = moyenne(x), moyenne(y) 
  
    # calculating cross-deviation and deviation about x 
    SS_xy = np.sum(y*x) - n*m_y*m_x 
    SS_xx = np.sum(x*x) - n*m_x*m_x 
  
    # calculating regression coefficients 
    b_1 = SS_xy / SS_xx 
    b_0 = m_y - b_1*m_x 
  
    return(b_0, b_1) 
  
def plot_regression_line(x, y, b): 
    # plotting the actual points as scatter plot 
    plt.scatter(x, y, color = "m", 
               marker = "o") 
  
    # predicted response vector 
    y_pred = b[0] + b[1]*x 
  
    # plotting the regression line 
    plt.plot(x, y_pred, color = "g") 
  
    # putting labels 
    plt.xlabel('x') 
    plt.ylabel('y') 
  
    # function to show plot 
    plt.show() 
  
def main(): 
 
  
    # estimating coefficients 
    b = estimate_coef(x, y) 
    print("Estimated coefficients:\nb_0 = {}  \ \nb_1 = {}".format(b[0], b[1])) 
  
    # plotting regression line 
    plot_regression_line(x, y, b) 
  
if __name__ == "__main__": 
    main() 