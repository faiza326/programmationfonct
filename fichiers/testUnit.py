import unittest
import pandas as pd
from urlpage import Urlpage as A
from calcul import Calcul as C
class testValidator(unittest.TestCase):
    
    def test_get_page(self):
        url="https://www.logic-immo.com/vente-immobilier-marseille-tous-codes-postaux,paris-75,nord-59,421_99,100_1,84_1/options/groupprptypesids=1,2,6,7,12,15/page="
        num=2
        fct=A.get_pages(url,num)
        listSize=len(fct)
        FAILURE = 'incorrect value'
        self.assertEqual(listSize,num,FAILURE)

    def test_moyenne(self):
        num=[3,3]
        self.assertNotEqual(C.moyenne(num),8)

    def test_median(self):
        num=[1,2,3]
        FAILURE = 'incorrect value'
        self.assertEqual(C.mediane(num),2,FAILURE)
    
    def test_estimate_coef(self):
        d = {'col1': [1, 2], 'col2': [3, 4]}
        df = pd.DataFrame(data=d)
        col1=df.iloc[0:len(df),0]
        col2=df.iloc[0:len(df),1]
        fct=C.estimate_coef(col1,col2)
        coef1=2
        coef2=1
        FAILURE = 'incorrect value'
        self.assertEqual(fct,(coef1,coef2),FAILURE)

if __name__ == '__main__':
    unittest.main()