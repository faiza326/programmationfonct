import time
from bs4 import BeautifulSoup 
from urllib.request import urlopen
from urlpage import Urlpage as A
from nettoyage import Nettoyage as B
from calcul import Calcul as C
import pandas as pd
from urllib.request import urlopen
import csv
import numpy as np
from functools import reduce


class Fich:
    
    def _init_(self):
        pass
    
    def get_info(response):
        file = open('donnees.csv', 'w',encoding='utf8',errors='ignore')
        writer = csv.writer(file)
        # write title row
        writer.writerow(["Ville", "Prix", "Superficie m²", "Piéces"])


        for res in response:
                soup = A.Fct_BeautifulSoup(res, 'html.parser')
                em_box = A.SoupFindAll(soup,"div","class","offer-details")

                for channel in em_box:
                    
                    Ville = A.SoupFindAtr(channel,"span","class","offer-details-location--locality")
                    Prix = (A.SoupFind(channel,"span")).text
                    Superficie = A.SoupFindAtr(channel,"span","class","offer-area-number") 
                    NbrChambre = (A.SoupFindAtr(channel,"span","class","offer-rooms-number"))
                    writer.writerow([Ville, Prix, Superficie, NbrChambre])
       
        file.close()
    
    def get_data(df):
            df.to_csv('donnees_clean.csv')
            d1=pd.read_csv('donnees_clean.csv')
            fic=d1.columns[0]
            d1=d1.drop([fic],axis=1)
            d1.drop_duplicates(inplace=True)
            d1.to_csv('donnees_clean.csv')
            df.to_csv('finalDonnees.csv',mode='a',header=False)
            d2=pd.read_csv('finalDonnees.csv')
            fic=d2.columns[0]
            d2=d2.drop([fic],axis=1)
            d2.drop_duplicates(inplace=True)
            d2.to_csv('finalDonnees.csv')
            df1=pd.read_csv('donnees_clean.csv')
            df2=pd.read_csv('finalDonnees.csv')
            return (df1,df2)